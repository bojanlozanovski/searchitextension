$(function(){
	
	console.log("popup.js is alive");
	
	chrome.tabs.query({"active":true, "currentWindow": true}, function(tabs){
		
		chrome.tabs.sendMessage(tabs[0].id, {todo: "giveWord"});
		
	});
	
	chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
		
		console.log("Event fired up.");

		
		if(request.todo == "sendWord" && request.text){
			
			$('#word').text(request.text);
			$('#proba').text('Searching... ');
			
			
			// 1. create a new XMLHttpRequest object -- an object like any other!
			var myRequest = new XMLHttpRequest();
			// 2. open the request and pass the HTTP method name and the resource as parameters
			//myRequest.open('GET', 'http://codepen.io/eclairereese/pen/BzQBzR.html');
			var url = 'http://127.0.0.1/Search/search.php';
			var fixedURL = fixedEncodeURI(url);
			var params = 'text='+request.text;
			myRequest.open('POST', fixedURL, true);
			
			//Send the proper header information along with the request
			myRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			// 3. write a function that runs anytime the state of the AJAX request changes
			myRequest.onreadystatechange = function () { 
				// 4. check if the request has a readyState of 4, which indicates the server has responded (complete)
				if (myRequest.readyState === 4) {
					
					
					chrome.storage.sync.get(['grammar', 'category', 'examples', 'addMetaData'], function(options){
						var showGrammar = options.grammar;
						var showCategory = options.category;
						var showExamples = options.examples;
						var showAddMetaData = options.addMetaData;

					
						// 5. insert the text sent by the server into the HTML of the 'ajax-content'
						var json = JSON.parse(myRequest.responseText);
						var dynamicHTML = '';
						
						//Get grammar field if there is one.
						if(showGrammar && decode_utf8(json.grammar).length > 0){
							dynamicHTML += '<p>' + decode_utf8(json.grammar) + '</p>';
						}
						//Get array of meanings
						if(json.meanings){
							var meanings = (json.meanings);
							for(var i = 0; i<meanings.length; i+=1){
								dynamicHTML += '<div class=\'definition\'>';
								//Get explanation of particular meaning
								if(decode_utf8(meanings[i].explanation)){
									var explanation = decode_utf8(meanings[i].explanation);
									dynamicHTML += '<b>' + explanation + '</b><br>'; 
								}
								
								//Get category field of particular meaning.
								if(showCategory && decode_utf8(meanings[i].category)){
									var category = decode_utf8(meanings[i].category);
									dynamicHTML += '<div class=\'prepend-1\'>' + category + '</div>'; 
								}
								
								//Get examples of particular meaning
								if(showExamples && meanings[i].examples.length > 0){
									dynamicHTML += '<span class=\'label\'>Примери:</span>';
									for(var j =0; j<meanings[i].examples.length; j+=1){
										if(decode_utf8(meanings[i].examples[j])){
											var example = decode_utf8(meanings[i].examples[j]);
											dynamicHTML += '<div class=\'example\'>' + example + '</div>'; 
										}
									}
								}
								
								//Get additionalMetaData field of particular meaning.
								if(showAddMetaData && decode_utf8(meanings[i].addMetaData)){
									var addMetaData = decode_utf8(meanings[i].addMetaData);
									dynamicHTML += '<div>' + addMetaData + '</div>'; 
								}
								dynamicHTML += '</div>';
							}
						}
						
						//In essence, check if nothing has been found...
						if(dynamicHTML.length == 0){
							dynamicHTML += '<p>' + 'Бараниот збор не е најден.' + '</p>';
						}
						
						//$('#word').text(myRequest.responseText);
						$('#proba').html(dynamicHTML);
					
					});//storage call
				}
			}
			myRequest.send(params);
		}
		
	});
	
});

function fixedEncodeURI(url){
	
	return encodeURI(url).replace('/%5B/g', "[").replace('/%5D/g', ']');
	
};

function encode_utf8(s) {
  return unescape(encodeURIComponent(s));
}

function decode_utf8(s) {
  return decodeURIComponent(escape(s));
}