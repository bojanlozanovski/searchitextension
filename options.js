$(function(){
	
	chrome.storage.sync.get(['grammar', 'category', 'examples', 'addMetaData'], function(options){
		$('#grammar').prop('checked', options.grammar);
		$('#category').prop('checked', options.category);
		$('#examples').prop('checked', options.examples);
		$('#addMetaData').prop('checked', options.addMetaData);

	});
	
	$('#saveOptions').click(function(){
		
		var grammar = $('#grammar').is(':checked');
		var category = $('#category').is(':checked');
		var examples = $('#examples').is(':checked');
		var addMetaData = $('#addMetaData').is(':checked');

		chrome.storage.sync.set({"category" : category, "grammar" : grammar, "examples" : examples, "addMetaData" : addMetaData}, function(){
			close();
		});
			
		
		
	});
	
});